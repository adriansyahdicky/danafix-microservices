package co.id.danafix.service.impl;

import co.id.danafix.dto.request.AddMemberRequest;
import co.id.danafix.dto.response.NewMemberResponseDTO;
import co.id.danafix.entity.Member;
import co.id.danafix.exception.ResourceNotFoundException;
import co.id.danafix.repository.MemberRepository;
import co.id.danafix.service.MemberService;
//import io.fabric8.kubernetes.client.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class MemberServiceImpl implements MemberService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberServiceImpl.class);

    @Autowired
    private MemberRepository memberRepository;

    @Override
    public Member getMemberById(Integer id) {
        return memberRepository.findById(id).orElseThrow(() -> new
                ResourceNotFoundException("Data Member when Id "+  id + " " + "cannot find in database"));
    }

    @Override
    public Page<Member> getMemberPage(Pageable pageable) {
        return memberRepository.getMemberPage(pageable);
    }

    @Override
    public NewMemberResponseDTO newMember(AddMemberRequest addMemberRequest) {
        try {
            NewMemberResponseDTO newMemberResponseDTO = new NewMemberResponseDTO();
            Member member = Member.builder()
                    .id(addMemberRequest.getId())
                    .uuid(UUID.randomUUID().toString())
                    .name(addMemberRequest.getName())
                    .mobile(addMemberRequest.getMobile())
                    .email(addMemberRequest.getEmail())
                    .ktp(addMemberRequest.getKtp())
                    .homeAddress(addMemberRequest.getHomeAddress())
                    .dateOfBirth(addMemberRequest.getDateOfBirth())
                    .placeOfBirth(addMemberRequest.getPlaceOfBirth())
                    .build();
            memberRepository.save(member);
            BeanUtils.copyProperties(member, newMemberResponseDTO);
            LOGGER.info("success register new member : " + newMemberResponseDTO);
            return newMemberResponseDTO;
        }
        catch (Exception ex){
            LOGGER.error("error register new member : " + ex.getMessage());
            throw new RuntimeException("Message Error "+ ex.getMessage());
        }
    }
}
