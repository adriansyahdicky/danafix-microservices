package co.id.danafix.service;

import co.id.danafix.dto.request.AddMemberRequest;
import co.id.danafix.dto.response.NewMemberResponseDTO;
import co.id.danafix.entity.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MemberService {
    Member getMemberById(Integer id);
    Page<Member> getMemberPage(Pageable pageable);
    NewMemberResponseDTO newMember(AddMemberRequest addMemberRequest);
}
