package co.id.danafix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication(scanBasePackages={"co.id.danafix"})
@EnableDiscoveryClient
@EnableSwagger2
public class DanafixCustomerCommunicationApplication {

	public static void main(String[] args) {
		SpringApplication.run(DanafixCustomerCommunicationApplication.class, args);
	}

	@Bean
	public Docket customerCommunicationProvider(){
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("co.id.danafix.controller"))
				.paths(PathSelectors.any())
				.build()
				.apiInfo(new ApiInfoBuilder().version("1.0").title("Costumer Communication API").description("Documentation Costumer Communication API v1.0").build());
	}

//	@Bean
//	public Docket swaggerCustomerCoomunicationApi10() {
//		return new Docket(DocumentationType.SWAGGER_2)
//				.select()
//				.apis(RequestHandlerSelectors.basePackage("co.id.danafix.controller"))
//				.paths(PathSelectors.any())
//				.build()
//				.apiInfo(new ApiInfoBuilder().version("1.0").title("Costumer Communication API").description("Documentation Costumer Communication API v1.0").build());
//	}
}
