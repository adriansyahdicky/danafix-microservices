package co.id.danafix.controller;

import co.id.danafix.dto.request.AddMemberRequest;
import co.id.danafix.dto.response.NewMemberResponseDTO;
import co.id.danafix.entity.Member;
import co.id.danafix.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/member")
public class MemberController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MemberController.class);

    @Autowired
    private MemberService memberService;

    @GetMapping(value = "/get-member-page")
    public ResponseEntity<?> getMemberPage(Pageable pageable){
        try{
            LOGGER.info("controller getMemberPage in");
            Page<Member> getDataMember = memberService.getMemberPage(pageable);
            LOGGER.info("data member success di ambil " + getDataMember.getContent());
            return ResponseEntity.ok(getDataMember);
        }catch (Exception ex){
            LOGGER.error("error controller getMemberPage : "+ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping(value = "/get-memberbyid/{id}")
    public ResponseEntity<?> getMemberById(@PathVariable("id") Integer id){
        try{
            Member getMemberById = memberService.getMemberById(id);
            return ResponseEntity.ok(getMemberById);
        }catch (Exception ex){
            LOGGER.error("error controller getMemberById : "+ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/new-member")
    public ResponseEntity<?> newMember(@RequestBody AddMemberRequest addMemberRequest){
        try{
            NewMemberResponseDTO newMemberResponseDTO = memberService.newMember(addMemberRequest);
            return ResponseEntity.ok(newMemberResponseDTO);
        }catch (Exception ex){
            LOGGER.error("error controller newMember : "+ex.getMessage());
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
