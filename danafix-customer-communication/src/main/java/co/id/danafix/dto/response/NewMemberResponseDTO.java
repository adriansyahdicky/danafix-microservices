package co.id.danafix.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NewMemberResponseDTO implements Serializable {

    private String uuid;

    private String name;

}
