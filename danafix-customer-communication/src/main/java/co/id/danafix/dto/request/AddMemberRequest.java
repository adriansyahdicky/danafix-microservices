package co.id.danafix.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddMemberRequest {
    private int id;
    private String name;
    private String mobile;
    private String email;
    private String ktp;
    private String homeAddress;
    private Date dateOfBirth;
    private String placeOfBirth;
}
