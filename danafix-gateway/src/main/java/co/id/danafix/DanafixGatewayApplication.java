package co.id.danafix;

import co.id.danafix.config.RibbonConfiguration;
import org.slf4j.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.ribbon.RibbonAutoConfiguration;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import javax.annotation.PostConstruct;
import java.util.stream.Stream;

@SpringBootApplication(scanBasePackages = {"co.id.danafix"})
@EnableDiscoveryClient
@EnableZuulProxy
@EnableSwagger2
@AutoConfigureAfter(RibbonAutoConfiguration.class)
@RibbonClients(defaultConfiguration = RibbonConfiguration.class)
public class DanafixGatewayApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(DanafixGatewayApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DanafixGatewayApplication.class, args);
	}

//	@Autowired
//	DiscoveryClient client;
//
//	@PostConstruct
//	public void init() {
//		LOGGER.info("Services: {}", client.getServices());
//		Stream<ServiceInstance> s = client.getServices().stream().flatMap(it -> client.getInstances(it).stream());
//		s.forEach(it -> LOGGER.info("Instance: url={}:{}, id={}, service={}", it.getHost(), it.getPort(), it.getInstanceId(), it.getServiceId()));
//	}

}
