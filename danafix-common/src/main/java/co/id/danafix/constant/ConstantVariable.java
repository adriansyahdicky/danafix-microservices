package co.id.danafix.constant;

public class ConstantVariable {
    public static String SUCCESS = "success";
    public static String FAILED = "failed";
    public static String ERROR = "error";
    public static String BADREQUEST = "bad request";
    public static String NOTFOUND = "not found";
    public static String CONFLICT = "conflict";
}
