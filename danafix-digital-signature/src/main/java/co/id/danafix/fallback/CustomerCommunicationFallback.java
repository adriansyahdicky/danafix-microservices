package co.id.danafix.fallback;

import co.id.danafix.client.CustomerCommunicationClient;
import feign.hystrix.FallbackFactory;
import org.slf4j.*;
import org.springframework.stereotype.Component;

@Component
public class CustomerCommunicationFallback implements FallbackFactory<CustomerCommunicationClient> {
    private static final Logger log = LoggerFactory.getLogger(CustomerCommunicationFallback.class);

    @Override
    public CustomerCommunicationClient create(Throwable cause) {
        if (log.isInfoEnabled()){
            log.debug("{\"class\" : \"CustomerCommunicationFallback\", " +
                    "\"default_message_error\" : \" "+cause.getMessage()+" \"," +
                    "\"error\" : \"danafix-customer-communication belum up atau belum terkoneksi !\"}");
        }
        return new CustomerCommunicationClientFallback(cause);
    }
}
