package co.id.danafix.fallback;

import co.id.danafix.client.CustomerCommunicationClient;
import feign.FeignException;

public class CustomerCommunicationClientFallback implements CustomerCommunicationClient {
    private final Throwable cause;

    public CustomerCommunicationClientFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public String getMemberById(Integer id) {
        if (cause instanceof FeignException && ((FeignException) cause).status() == 404){
            return null;
        }
        return null;
    }
}
