package co.id.danafix.client;

import co.id.danafix.fallback.CustomerCommunicationFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "danafix-customer-communication", fallbackFactory = CustomerCommunicationFallback.class)
public interface CustomerCommunicationClient {

    @GetMapping(value = "/api/member/get-memberbyid/{id}")
    String getMemberById(@PathVariable("id") Integer id);

}
