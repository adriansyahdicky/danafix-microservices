package co.id.danafix.controller;

import co.id.danafix.service.DigitalSignatureService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/digital-signature")
public class DigitalSignatureController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DigitalSignatureController.class);

    @Autowired
    private DigitalSignatureService digitalSignatureService;

    @GetMapping(value = "/checkingSertificate/{id}")
    public ResponseEntity<?> checkingSertificate(@PathVariable("id") Integer id){
        try{
            boolean cheking_false = digitalSignatureService.checkSertificate(id);
            if(cheking_false == true)
                return ResponseEntity.ok("Sudah Nyambung Dengan Feign Client !!!!");
            else
                return ResponseEntity.ok("Data Tidak ditemukan !!!!");
        }catch (Exception e){
            LOGGER.error("Error di method checkingSertificate : " + e.getMessage());
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
