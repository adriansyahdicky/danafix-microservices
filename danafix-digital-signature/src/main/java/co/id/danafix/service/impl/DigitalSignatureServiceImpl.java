package co.id.danafix.service.impl;

import co.id.danafix.client.CustomerCommunicationClient;
import co.id.danafix.service.DigitalSignatureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DigitalSignatureServiceImpl implements DigitalSignatureService {

    @Autowired
    CustomerCommunicationClient customerCommunicationClient;


    @Override
    public boolean checkSertificate(Integer id) {
        String customercommunication_client = customerCommunicationClient.getMemberById(id);
        if(customercommunication_client == null){
            return false;
        }
        return true;
    }
}
