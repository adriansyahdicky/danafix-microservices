package co.id.danafix.service;

public interface DigitalSignatureService {
    boolean checkSertificate(Integer id);
}
